from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from todos.models import TodoItem, TodoList

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields= ["name"]
    success_url = reverse_lazy("todo_list")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list")

    # def get_queryset(self):
    #     return TodoList.objects.filter(author=self.request.user)


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ["task","due_date", "is_completed","list"]
    success_url = reverse_lazy("todo_list")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/edit.html"
    fields = ["task","due_date","is_completed","list"]
    success_url = reverse_lazy("todo_list")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)
