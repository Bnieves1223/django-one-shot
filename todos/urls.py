from django.urls import path
from todos.models import TodoItem

from todos.views import TodoItemCreateView, TodoListListView, TodoItemUpdateView,TodoListDeleteView,TodoListDetailView,TodoListCreateView,TodoListUpdateView



urlpatterns = [
path("",TodoListListView.as_view(), name="todo_list"),
path("<int:pk>/",TodoListDetailView.as_view(), name="todo_detail"),
path("create/",TodoListCreateView.as_view(),name = "todo_create"),
path("<int:pk>/edit/",TodoListUpdateView.as_view(),name="todo_edit"),
path("<int:pk>/delete/",TodoListDeleteView.as_view(),name="todo_delete"),
path("items/create/",TodoItemCreateView.as_view(), name= "items_create"),
path("items/<int:pk>/edit/",TodoItemUpdateView.as_view(),name="items_edit")

]